import Deck
import Player
import operator

class State:
    all_in, wait, check, call, raised, folded = range(6)

class Game_Step:
    pf, f, t, r = range(4)

class Jerarquia:
    HCard, Pair, DPair, Trips, Straight, Flush, Full, Poker, Straight_Flush = range(9)

class Valor:
    Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace = range(13)

class GameManager:
    _hand_id = 0
    _ended = False
    _nplayers = 0
    _n_active_players = 0
    _bb_size = 0
    _max_entry = 100


    _deck = Deck.Deck()
    _players = []
    _players_on_table = [] #TODO

    _game_step = Game_Step.pf
    _shownCards = False
    _common_cards = []
    _pot = 0
    _highest_bet = 0
    _active_player = 0

    def __init__(self, players, size):
        self._hand_id = 0
        self._ended = False
        self._nplayers = players
        self._n_active_players = players
        self._bb_size = size
        self._deck = Deck.Deck()
        self._pot = 0        
        self._game_step = Game_Step.pf
        _shownCards = False
        for x in range (0, players):
            player = Player.Player(x, x, size*100, size)
            self._players.append(player)

    def players_to_talk(self):
        count = 0
        for x in self._players:
            if(x._state == State.wait):
                count += 1
        return count

    def next_player(self):
        self._active_player += 1
        self._active_player = self._active_player % self._nplayers

    def __Hand__(self):
        self.new_hand()
        self.__preflop__()
        self.__flop__()
        self.__turn__()
        self.__river__()
        

    def new_hand(self):
        #Restarting variables
        self._common_cards.clear()  #Restarting board
        self._deck = Deck.Deck()    #Shuffling deck
        self._highest_bet = 0.0      #Empty pots
        self._pot = 0
        self._ended = False 
        
        #HandID
        self._hand_id +=1
        #Advancing the position
        for x in range(0, self._nplayers):
            self._players[x].advancePosition(self._nplayers)
        #Updating players on list
        self._players.sort()        
        #Asigning cards to players
        self.asign_cards()
        #Setting up blinds
        for x in range(0, 2):
            self._pot += self._players[x].putBlind() 
            self.next_player()
        self._highest_bet = self._bb_size  
    
    def __preflop__(self):
        #Bets        
        self.bet_turns()
        self.clean_states()    

    def __flop__(self):
        if(not self._ended):
            self._game_step = Game_Step.f
            self._common_cards.append(self._deck.pop())      
            self._common_cards.append(self._deck.pop())
            self._common_cards.append(self._deck.pop())
            print("Flop: ", self._common_cards)
            #Bets        
            self.bet_turns()
            self.clean_states() 

    def __turn__(self) :
        if(not self._ended):
            self._game_step = Game_Step.t
            self._common_cards.append(self._deck.pop())
            print("Turn: ", self._common_cards)
            #Bets        
            self.bet_turns()
            self.clean_states() 
    
    def __river__(self) :
        if(not self._ended):
            self._game_step = Game_Step.r
            self._common_cards.append(self._deck.pop())
            print("River: ", self._common_cards)
            #Bets        
            self.bet_turns()
            self.clean_states() 

    def asign_cards(self):
        for x in range (0, self._nplayers):
            self._players[x].setC1(self._deck.deck.pop())
        for x in range (0, self._nplayers):
            self._players[x].setC2(self._deck.deck.pop())

    def get_last(self, l, n):
        ret = -1
        for x in range(0, len(l)):
            if(l[x] == n):
                ret = x
        return ret        

    
    def bet_turns(self):
        while(self.players_to_talk() != 0):
            #print("Este jugador está: ", self._players[self._awaiting_answer]._state)
            #Si no ha foldeado o si no está all_in, continua la mano
            if(self._players[self._active_player]._state == State.wait): 
                #Pedir respuesta al jugador
                answer, bet = self._players[self._active_player].waitPreflopAction(self._highest_bet)
                if(answer == State.raised): #Si el jugador sube
                    self.raising(bet, self._active_player)
                elif(answer == State.call): #Si el jugador paga
                    self.calling(bet, self._active_player)
                elif(answer == State.folded):
                    self.folding(self._active_player)
                else: #Si ha pasado, se pasa el turno al siguiente jugador
                    self.checked(self._active_player)
            else: #Si ha foldeado, se pasa el turno al siguiente jugador
                    self.next_player()               
        self.checkShowdown()

    def raising(self, bet, active_player):
        self._highest_bet = bet #Se actualiza la apuesta más alta
        self._pot += bet #Se actualiza el valor del bote
        for x in range (0, self._nplayers):
            if (x!=active_player and self._players[x]._state != State.folded and self._players[x]._state != State.all_in):
                self._players[x]._state = State.wait
        self.next_player()

    def calling(self, bet, active_player):
        self._pot += bet              
        self.next_player()

    def folding(self, active_player):
        self._n_active_players -=1
        self.next_player()
    
    def checked(self, active_player):
        self.next_player()

    def checkShowdown(self):
        #Comprueba si solo queda 1 jugador activo
        if(len(self.playersAlive()) == 1 and not self._ended):
            self.win(self.playersAlive()[0]) #Gana el jugador activo
        #Comprueba si todos los jugadores están all in
        if(self.playersAllIn()):
            #Mostrar Cartas
            if(not self._shownCards):
                self.showCards()
            #Calcular % de victoria de cada jugador

        #Comprueba si es el river. En caso de serlo, showdown normal
        if(self._game_step == Game_Step.r and not self._ended):
            #Mostrar cartas
            if(not self._shownCards):
                self.showCards()
            #Calcular fuerza de la mano de cada jugador
            for x in self.playersAlive():
                self.handStrengh(x)
            #Calcular el jugador que gana
            winner = self.winnerPlayer(self.playersAlive()) #No comprueba empates
            self.win(winner)
    
    
    def winnerPlayer(self, players):
        #TODO: COMPROBAR MEJOR EMPATES EN BOTES MULTIJUGADOR
        ret = players[0]
        for x in range(1, len(players)):
            if(players[x]._score > ret._score):
                ret = players[x]
            elif(players[x]._score == ret._score):
                ret = -1
        return ret

    def win(self, player):        
        self._ended = True
        #En caso de empate, se reparte el bote
        if(player == -1):
            pa = self.playersAlive()
            for x in pa:
                x._stack += self._pot/len(pa)
        else:
            player._stack += self._pot
            print("El jugador ", player._id , "gana ", self._pot)

    def nameHand(self, player):
        handnames = ["Carta alta al", "Pareja de", "Dobles parejas de", "Trío de", "Escalera al", "Color al", "Full de", "Poker de", "Escalera de color al"]
        denames = ["Ases", "Doses", "Treses", "Cuatros", "Cincos", "Seises", "Sietes", "Ochos", "Nueves", "Dieces", "Jotas", "Reinas", "Reyes", "Ases"]
        alnames = ["Ases", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Diez", "J", "Q", "K", "As"]
        
        aux_name = player._score._jerarquia
        if (aux_name == 0 or aux_name == 4 or aux_name == 5 or aux_name == 8):
            print("El jugador ", player._id, " tiene: ", handnames[aux_name], alnames[player._score._valor1])
        elif(aux_name == 1 or aux_name == 3 or aux_name == 7):
            print("El jugador ", player._id, " tiene: ", handnames[aux_name], denames[player._score._valor1])
        else:
            print("El jugador ", player._id, " tiene: ", handnames[aux_name], denames[player._score._valor1], denames[player._score._valor2])

    def showCards(self):
        self._shownCards = True
        for x in self.playersAlive():
            print(x._c1, ", ", x._c2)

    def playersAllIn(self):
        l = self.playersAlive()
        ret = True
        for x in l:
            if(x._state != State.all_in):
                ret = False
                break
        return ret

    def playersAlive(self):
        pA = []
        for x in self._players:
            if(x._state != State.folded):
                pA.append(x)
        return pA

    def clean_states(self):        
        self._highest_bet = 0 #Se resetean los montos de apuestas
        self._awaiting_answer = 0 #Se pone como el jugador a actuar a la SB
        for x in self._players: #Si el jugador no está all in o foldeado, se pone en espera
            if(x._state != State.folded and x._state != State.all_in):
                x._state = State.wait
                x._bet = 0.0

    def handStrengh(self, player):
        aux = self._common_cards.copy()        
        aux.append(player._c1)
        aux.append(player._c2)                

        numeros = []
        palos = []

        ultimate = []
        ultimate_palos = []
        #Declara 2 vectores auxiliares para ayudar a calcular las manos
        for x in aux:
            numeros.append(x[0])
            palos.append(x[1])
        #Inicializa el vector definitivo
        for x in range(0, 14):
            ultimate.append(0)
            ultimate_palos.append("")
        #Llena el vector definitivo
        count = 0
        for x in numeros:
            if(x == 'T'):
                ultimate[9] += 1
                ultimate_palos[9] = palos[count] #Saca el primer palo, correspondiente al primer número   
                count += 1        
            elif(x == 'J'):
                ultimate[10] += 1
                ultimate_palos[10] = palos[count] #Saca el primer palo, correspondiente al primer número   
                count += 1
            elif(x == 'Q'):
                ultimate[11] += 1
                ultimate_palos[11] = palos[count] #Saca el primer palo, correspondiente al primer número   
                count += 1
            elif(x == 'K'):
                ultimate[12] += 1
                ultimate_palos[12] = palos[count] #Saca el primer palo, correspondiente al primer número   
                count += 1
            elif(x == 'A'):                
                ultimate[13] += 1
                ultimate_palos[13] = palos[count] #Saca el primer palo, correspondiente al primer número   
                count += 1
            else:
                ultimate[int(x)-1] += 1
                ultimate_palos[int(x)-1] = palos[count] #Saca el primer palo, correspondiente al primer número   
                count += 1

        #Si existe un 2 en el vector, hay una pareja, si hay 2 o más, hay doble pareja
        if(ultimate.count(2) == 1):
            player._score._jerarquia = Jerarquia.Pair
            player._score._valor1 = ultimate.index(2)
        if(ultimate.count(2) > 1):
            player._score._jerarquia = Jerarquia.DPair
            #Obteniendo la pareja más alta y luego la segunda pareja
            player._score._valor1 = self.get_last(ultimate, 2)
            player._score._valor2 = ultimate.index(2) #Hay que revisar que no haya 3 parejas
        if(ultimate.count(3) >= 1):
            #Comprobar si solo hay trio
            if (ultimate.count(2) == 0 and ultimate.count(3) == 1):
                #Solo trio
                player._score._jerarquia = Jerarquia.Trips
                player._score._valor1 = ultimate.index(3)
            else:
                #Full
                player._score._jerarquia = Jerarquia.Full
                player._score._valor1 = self.get_last(ultimate, 3)
                try:
                    player._score._valor2 = ultimate.index(2)
                except:
                    player._score._valor2 = ultimate.index(3)
        if(ultimate.count(4) == 1):
            player._score._jerarquia = Jerarquia.Poker

        palo = ''
        #Comprobando si hay color
        if(palos.count('s') >= 5):
            player._score._jerarquia = Jerarquia.Flush            
            palo = 's'
        elif(palos.count('c') >= 5):
            player._score._jerarquia = Jerarquia.Flush
            palo = 'c'
        elif(palos.count('h') >= 5):
            player._score._jerarquia = Jerarquia.Flush
            palo = 'h'
        elif(palos.count('d') >= 5):
            player._score._jerarquia = Jerarquia.Flush
            palo = 'd'

        #Comprobando si hay escalera
        #Se asigna a la posición 0 el mismo valor que a 14, ya que ambas representarán aquí el as
        ultimate[0] = ultimate[13]        
        count = 0 #Se usa para comprobar cuantas cartas consecutivas hay
        fcount = 0
        high_card = 0 #Se usa para conocer a qué carta está la escalera
        for x in range(0, 14):
            if(ultimate[x] > 0):
                count +=1
                if(ultimate_palos[x] == palo):
                    fcount +=1
                else:
                    fcount = 0
            else:
                count = 0
        #Si hay escalera y color, hay escalera de color
        if(count >= 5 and fcount >= 5):
            high_card = x
            if(high_card != 0):
                #Hay escalera de color, asignando puntuación
                player._score._jerarquia = Jerarquia.Straight_Flush
                player._score._valor1 = x-1 #-1 porque la X comienza en con el as, mientras que los valores empiezan con el 2
        #Si no, hay escalera solamente
        elif(count >= 5):
            high_card = x
            if(high_card != 0):
                #Hay escalera, asignando puntuación
                player._score._jerarquia = Jerarquia.Straight
                player._score._valor1 = x-1 #-1 porque la X comienza en con el as, mientras que los valores empiezan con el 2

        
        #Una vez determinada la puntuación, calcular kickers
        #Si hay una pareja, hay 3 kickers
        if(player._score._jerarquia == Jerarquia.Pair):
            count = 0
            for x in range (13, 0, -1):
                if (count >= 3):
                    break
                if (ultimate[x] == 1):
                    player._score._kickers.append(x)
                    count += 1
        #Si hay dobles parejas, hay 1 kicker
        if(player._score._jerarquia == Jerarquia.DPair):            
            for x in range (13, 0, -1):
                if (ultimate[x] == 1):
                    player._score._kickers.append(x)
                    count += 1
                    break
        #Si hay un trío, hay 2 kickers
        if(player._score._jerarquia == Jerarquia.Trips):
            count = 0
            for x in range (13, 0, -1):
                if (count >= 2):
                    break
                if (ultimate[x] == 1):
                    player._score._kickers.append(x)
                    count += 1
        #Si hay color, hay 5 kickers, no muy pulido, revisar
        if(player._score._jerarquia == Jerarquia.Flush):     
            count = 0               
            for x in aux:                
                if (x[1] == palo and count < 5):
                    player._score._kickers.append(x)
                    count += 1
                    
        #Si hay poker, hay 1 kicker
        if(player._score._jerarquia == Jerarquia.Poker):                    
            for x in range (13, 0, -1):
                if (ultimate[x] >= 1 ):
                    player._score._kickers.append(x)
                    count += 1
                    break

        #Si llega aquí sin asignar jerarquía, es que tiene carta alta
        if(player._score._jerarquia == Jerarquia.HCard):
            count = 0               
            for x in range (13, 0, -1):
                if (count >= 5):
                    break                
                if (ultimate[x] == 1 and count == 0):
                    player._valor1 = x
                    count += 1
                elif(ultimate[x] == 1 and count == 1):
                    player._valor2 = x
                    count += 1
                else:
                    player._score._kickers.append(x)
                    count += 1
        self.nameHand(player)
        return player._score        

gm = GameManager(6, 0.5)
gm.__Hand__()

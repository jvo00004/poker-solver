import random
import Score
class Positions:
    sb, bb, utg, mp, co, bu = range(6)

class State:
    all_in, wait, check, call, raised, folded = range(6)

class Player:
    #ID
    _id = ""
    #Cards
    _c1 = ""
    _c2 = ""
    #Position
    _position = 0
    #Stack & BBs
    _stack = 0.0
    _stack_bb = 0.0
    #State (playing or folded)
    _state = State.wait
    #Blinds size
    _blind = 0.0
    #Actual bet
    _bet = 0.0
    #Hand Score
    _score = Score.Score()

    def __init__(self, id, position, stack, blind):
        self._id = id
        self.setPosition(position)
        self._stack = stack
        self._blind = blind
        self._score = Score.Score()
        self._state = State.wait       

    def __lt__(self, other):
        return self._position < other._position

    def setPosition(self, position):
        self._position = position

    def getStack(self):
        return self._stack

    def setStack(self, stack):
        self._stack = stack
        #TODO: Actualizar _stack_bb

    def addStack(self, pot):
        self._stack += pot
        #TODO: Actualizar _stack_bb

    def bet(self, size):
        ret = -1
        if(size < self._stack): #Normal Bet
            self._stack -= size
            self._bet += size
            #TODO: Actualizar _stack_bb
            ret = size            
        else: #GOES ALL IN
            ret = self._stack
            self._bet += self._stack 
            self._stack = 0
            self._state = State.all_in
            #TODO: Actualizar _stack_bb
        return ret
        
    def setC1(self, c1):
        self._c1 = c1
    def setC2(self, c2):
        self._c2 = c2

    def nextHand(self):
        self._c1 = ""
        self._c2 = ""
        self._state = State.wait

    def advancePosition(self, maxplayers):
        self.setPosition((self._position - 1) % maxplayers)

    def putBlind(self):
        if (self._position is Positions.sb):            
            return self.bet(self._blind/2)
        elif (self._position is Positions.bb):            
            return self.bet(self._blind)
        else:
            return 0

    def waitPreflopAction(self, bet_size):
        ret = random.randint(0, 3)
        if(ret == 0):            
            return self.fold(bet_size)            
        elif (ret == 1):
            return self.call(bet_size)            
        else:
            return self.raising(bet_size)

    def fold(self, bet_size):
        if (bet_size > 0):
            self._state = State.folded
            print("El jugador", self._id, "foldea")
            return State.folded, 0
        else:
            self._state = State.check
            print("El jugador", self._id, "pasa")
            return State.check, 0

    def call(self, bet_size):
        size = 0
        if(bet_size > self._bet and bet_size < (self._stack-self._bet)): #Normal Call
            size = bet_size - self._bet
            self.bet(size)
            self._state = State.call
            print("El jugador", self._id, "paga ", size)
            return State.call, size
        elif(bet_size > self._bet and bet_size >= (self._stack-self._bet)): #Call All in
            size = self._stack
            self.bet(self._stack)
            print("El jugador", self._id, "paga all in de ", size)
            if(self._stack == 0.0): 
                self._state = State.all_in
                return State.call, size
            else:  
                self._state = State.call 
                return State.call, size
        else: #Si la apuesta es 0, se checkea
            return self.fold(bet_size)

        

    def raising(self, bet_size): #Actualmente solo soporta min-raises  
        size = 0.0              
        if(bet_size == 0): #aquí se controla el post-flop
            self._state = State.raised        
            size = self.bet(self._blind)
            print("El jugador", self._id, "sube a", size)
        elif(bet_size > self._stack):#Si la apuesta es mayor que tu monto, pagas el all in
            return self.call(bet_size)
        elif(bet_size*2 > self._stack):#Si no puedes llegar al doble, all in            
            self._state = State.all_in
            size = self.bet(self._stack)            
            print("El jugador", self._id, "va all in con", size)
        else:     
            self._state = State.raised       
            size = self.bet(bet_size*2)                        
            print("El jugador", self._id, "sube a", size)
        return State.raised, size 
    
        



        

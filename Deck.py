import os
import random

class Deck:
    clubs = ["Ac", "Kc", "Qc", "Jc", "Tc", "9c", "8c", "7c", "6c", "5c", "4c", "3c", "2c"]
    spades = ["As", "Ks", "Qs", "Js", "Ts", "9s", "8s", "7s", "6s", "5s", "4s", "3s", "2s"]
    hearts = ["Ah", "Kh", "Qh", "Jh", "Th", "9h", "8h", "7h", "6h", "5h", "4h", "3h", "2h"]
    diamonds = ["Ad", "Kd", "Qd", "Jd", "Td", "9d", "8d", "7d", "6d", "5d", "4d", "3d", "2d"]

    deck = []

    def shuffle(self):
        random.shuffle(self.deck)
        #print(self.deck)

    def pop(self):
        return self.deck.pop()

    def __init__(self):
        for x in range (0, 13):
            self.deck.append(self.clubs[x])
            self.deck.append(self.spades[x])
            self.deck.append(self.hearts[x])
            self.deck.append(self.diamonds[x])
        #print(self.deck)
        self.shuffle()
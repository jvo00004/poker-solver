class Jerarquia:
    HCard, Pair, DPair, Trips, Straight, Flush, Full, Poker, Straight_Flush = range(9)

class Valor:
    Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace = range(13)

class Score:
    _jerarquia = 0
    _valor1 = 0
    _valor2 = 0
    _kickers = []

    def __init__(self):
        self._jerarquia = 0
        self._valor1 = -1
        self._valor2 = -1
        self._kickers = []

    def __eq__(self, cmp):
        if(self._jerarquia == cmp._jerarquia and self._valor1 == cmp._valor1 and self._valor2 == cmp._valor2):
            for x in range(0, len(self._kickers)):
                if(self._kickers[x] != cmp._kickers[x]):
                    return False
            return True   
        return False                 
            

    def __lt__(self, cmp):
        if(self._jerarquia != cmp._jerarquia):
            return self._jerarquia < cmp._jerarquia
        elif(self._valor1 != cmp._valor1):
            return self._valor1 < cmp._valor1
        elif(self._valor2 != cmp._valor2):
            return self._valor2 < cmp._valor2
        else:
            for x in range (0, len(self._kickers)):
                if(self._kickers[x] != cmp._kickers[x]):
                    return self._kickers[x] < cmp._kickers[x]
    